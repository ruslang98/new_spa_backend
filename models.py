from db import db

from flask_restful_swagger import swagger
from flask_bcrypt import generate_password_hash, check_password_hash


@swagger.model
class User(db.Model):
    """User model"""
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(256), unique=True, nullable=False)
    password = db.Column(db.String(256), unique=True, nullable=False)
    first_name = db.Column(db.String(256), nullable=True)
    second_name = db.Column(db.String(256), nullable=True)
    patronymic = db.Column(db.String(256), nullable=True)
    age = db.Column(db.Integer, nullable=True)
    sex = db.Column(db.String(5), nullable=True)
    position = db.Column(db.String(256), nullable=True)
    departament = db.Column(db.String(256), nullable=True)
    privacy_policy = db.Column(db.Boolean, default=False)

    def __init__(self, username, password):

        self.username = username
        self.password = password

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf-8')

    def check_password(self, password):
        return check_password_hash(self.password, password)


@swagger.model
class Task(db.Model):
    """Task model for users"""
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(256))
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    status = db.Column(db.String)
    director_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    responsible_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, text, start_date, end_date, status, director_id, responsible_id):
        self.text = text
        self.start_date = start_date
        self.end_date = end_date
        self.status = status
        self.director_id = director_id
        self.responsible_id = responsible_id






