"""Initial migration

Revision ID: 8e4821483e33
Revises: 
Create Date: 2020-10-16 23:22:23.130059

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8e4821483e33'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
