from flask_restful import Resource, request
from flask_restful_swagger import swagger

from serializers import user_schema, users_schema

from models import User

from db import db


class UserListAPI(Resource):
    """Endpoint to GET users."""
    @swagger.operation(
        notes='GET list of users.',
        responseMessages=[
            {
                "code": 200,
                "message": "OK."
            }
        ]
    )
    def get(self):
        all_users = User.query.all()

        result = users_schema.dump(all_users)
        return users_schema.jsonify(result)


class UserDetail(Resource):
    """Endpoint to GET, PATCH, DELETE user by id."""

    @swagger.operation(
        notes='GET task by id.',
        responseMessages=[
            {
                "code": 200,
                "message": "OK"
            }
        ]
    )
    def get(self, id):
        """Returns details of a user."""
        user = User.query.get_or_404(id)
        return user_schema.jsonify(user)

    @swagger.operation(
        notes='Update user by id.',
        parameters=[
            {
                "name": "body",
                "required": True,
                "allowMultiple": False,
                "dataType": User.__name__,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "OK."
            },
            {
                "code": 400,
                "message": "Bad request."
            },
            {
                "code": 404,
                "message": "Not found."
            }
        ]
    )
    def patch(self, id):
        """Update user."""
        # user = User.query.get_or_404(id)
        body = request.get_json()
        # user.update(**body)
        new_user = User.query.filter_by(id=id).update(**body).first()
        return user_schema.jsonify(new_user)

    @swagger.operation(
        notes='DELETE user by id.',
        responseMessages=[
            {
                "code": 204,
                "message": "Deleted."
            },
            {
                "code": 404,
                "message": "Not found."
            }
        ]
    )
    def delete(self, id):
        """Delete user."""
        user = User.query.get_or_404(id)
        db.session.delete(user)
        db.session.commit()
        return '', 204