from datetime import datetime

from flask import jsonify
from flask_restful import Resource, request
from flask_restful_swagger import swagger


from db import db
from models import Task
from models import User
from serializers import task_schema, tasks_schema
from serializers import user_schema, users_schema


class SearchAPI(Resource):
    def get(self):
        query = request.args.get('query')
        users_filter = User.query.filter(User.username.like(f'{query}%') or User.second_name.like(f'{query}%')).all()
        return users_schema.jsonify(users_filter)