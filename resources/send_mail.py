from flask import jsonify
from flask_restful import Resource, request
from flask_restful_swagger import swagger

from flask_mail import Mail, Message

mail = Mail()


class SendMailAPI(Resource):
    """Endpoint to POST mail."""

    @swagger.operation(
        notes='POST to send mail. '
              'Json example: '
              '{"mail_addresses": "email@mail.ru",'
              '"message_subject": "Some subject",'
              '"message_text": "Some text"}',
        responseMessages=[
            {
                "code": 201,
                "message": "Message has been sent."
            },
            {
                "code": 400,
                "message": "Bad request."
            }
        ]
    )
    def post(self):
        """Send some mail."""
        message_text = request.json['message_text']
        message_subject = request.json['message_subject']
        mail_addresses = request.json['mail_addresses']
        msg = Message(
            message_subject,
            sender='yourId@gmail.com',
            recipients=[mail_addresses]
        )
        msg.body = message_text
        mail.send(msg)
        return jsonify({'mail_addresses': mail_addresses,
                        'message_subject': message_subject,
                        'message:text': message_text})
