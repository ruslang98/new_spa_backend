from flask_restful import Resource, request
from flask_restful_swagger import swagger
from flask_jwt_extended import create_access_token


from models import User

from db import db


class SignupAPI(Resource):
    """Endpoint to sign up user."""

    @swagger.operation(
        notes='POST to sign up user.',
        parameters=[
            {
                "name": "body",
                "description": "",
                "required": True,
                "allowMultiple": False,
                "dataType": User.__name__,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 201,
                "message": "Created."
            },
            {
                "code": 400,
                "message": "Bad request."
            }
        ],

    )
    def post(self):
        """Sign up user."""
        username = request.json['username']
        password = request.json['password']
        if User.query.filter_by(username=username).first():
            return {
                "detail": "This user already exists"
            }, 400
        new_user = User(username, password)
        new_user.hash_password()
        db.session.add(new_user)
        db.session.commit()
        return {'id': new_user.id,
                'username': new_user.username,
                'password': new_user.password,
                }, 200


class SigninAPI(Resource):
    """Endpoint to sign in user."""

    @swagger.operation(
        notes='POST to sign in user. Use {"username":"username", "password":"password"} to sign in and get token.',
        responseMessages=[
            {
                "code": 201,
                "message": "Created."
            },
            {
                "code": 400,
                "message": "Bad request."
            }
        ],

    )
    def post(self):
        """Sign in user. Returns token auth."""
        username = request.json['username']
        password = request.json['password']
        user = User.query.filter_by(username=username).first()
        authorized = user.check_password(password)
        if not authorized:
            return {'error': 'email or password invalid'}, 401
        access_token = create_access_token(identity=str(user.id))
        return {'id': user.id,
                'token': access_token}, 200