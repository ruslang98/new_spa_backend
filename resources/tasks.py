from datetime import datetime

from flask import jsonify
from flask_restful import Resource, request
from flask_restful_swagger import swagger


from db import db
from models import Task
from serializers import task_schema, tasks_schema


class TaskListAPI(Resource):
    """Endpoint to GET or POST tasks."""
    @swagger.operation(
        notes='GET list of tasks.',
        responseMessages=[
            {
                "code": 200,
                "message": "OK."
            }
        ]
    )
    def get(self):
        """Returns list of tasks."""

        all_tasks = Task.query.all()
        result = tasks_schema.dump(all_tasks)
        return tasks_schema.jsonify(result)

    @swagger.operation(
        notes='POST to add task.',
        parameters=[
            {
                "name": "body",
                "description": "Date format : YYYY-MM-DD",
                "required": True,
                "allowMultiple": False,
                "dataType": Task.__name__,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 201,
                "message": "Created."
            },
            {
                "code": 400,
                "message": "Bad request."
            }
        ],

    )
    def post(self):
        """Create new task."""

        text = request.json['text']
        start_date = datetime.strptime(request.json['start_date'], "%Y-%m-%d")
        end_date = datetime.strptime(request.json['end_date'], "%Y-%m-%d")
        status = request.json['status']
        director_id = request.json['director_id']
        responsible_id = request.json['responsible_id']

        new_task = Task(text, start_date, end_date, status, director_id, responsible_id)

        db.session.add(new_task)
        db.session.commit()
        return jsonify({'id': new_task.id,
                        'text': new_task.text,
                        'start_date': new_task.start_date,
                        'end_date': new_task.end_date,
                        'status': new_task.status,
                        'director_id': new_task.director_id,
                        'responsible_id': new_task.responsible_id
                        })


class TaskDetailAPI(Resource):
    """Endpoint to GET, PATCH, DELETE task by id."""
    @swagger.operation(
        notes='GET task by id.',
        responseMessages=[
            {
                "code": 200,
                "message": "OK"
            }
        ]
    )
    def get(self, id):
        """Returns details of a task."""
        task = Task.query.get_or_404(id)
        return task_schema.jsonify(task)

    @swagger.operation(
        notes='Update task by id.',
        parameters=[
            {
                "name": "body",
                "description": "Date format : YYYY-MM-DD",
                "required": True,
                "allowMultiple": False,
                "dataType": Task.__name__,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "OK."
            },
            {
                "code": 400,
                "message": "Bad request."
            },
            {
                "code": 404,
                "message": "Not found."
            }
        ]
    )
    def patch(self, id):
        """Update task."""
        task = Task.query.get_or_404(id)
        body = request.get_json()
        task.update(**body)
        db.session.commit()
        return task_schema.jsonify(task)

    @swagger.operation(
        notes='DELETE task by id.',
        responseMessages=[
            {
                "code": 204,
                "message": "Deleted."
            },
            {
                "code": 404,
                "message": "Not found."
            }
        ]
    )
    def delete(self, id):
        """Delete task."""
        task = Task.query.get_or_404(id)
        db.session.delete(task)
        db.session.commit()
        return '', 204
