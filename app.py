import config
import logging

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_script import Manager
from flask_restful import Api
from flask_restful_swagger import swagger
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from flask_migrate import Migrate, MigrateCommand

from db import db
from cors import disable_cors

from resources.auth import (
    SignupAPI,
    SigninAPI,
)

from resources.users import (
    UserListAPI,
    UserDetail
)

from resources.tasks import (
    TaskListAPI,
    TaskDetailAPI
)

from resources.send_mail import (
    SendMailAPI
)

from resources.global_search import (
    SearchAPI
)

app = Flask(__name__)
app.debug = True
app.config.from_object(config.DefaultConfig)
manager = Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)
app.config['CORS_HEADERS'] = 'Content-Type'
disable_cors(app)
api = swagger.docs(Api(app), apiVersion='0.1', api_spec_url='/api/docs')
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
mail = Mail(app)

logging.getLogger('flask_cors').level = logging.DEBUG


@app.before_first_request
def create_tables():
    db.create_all()


db.init_app(app)

api.add_resource(SignupAPI, '/auth/signup/')
api.add_resource(SigninAPI, '/auth/signin/')

api.add_resource(UserListAPI, '/users/')
api.add_resource(UserDetail, '/users/<id>/')

api.add_resource(TaskListAPI, '/tasks/')
api.add_resource(TaskDetailAPI, '/tasks/<id>/')

api.add_resource(SendMailAPI, '/sendmail/')

api.add_resource(SearchAPI, '/search/')

if __name__ == '__main__':
    manager.run()
