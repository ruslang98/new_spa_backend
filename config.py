import os


class DefaultConfig():
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = 'ruslan.gonataev98@@gmail.com'
    MAIL_PASSWORD = '*********'
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    basedir = os.path.abspath(os.path.dirname(__file__))
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'db.sqlite3')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
