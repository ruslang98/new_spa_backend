from flask_marshmallow import Marshmallow

from models import User, Task

ma = Marshmallow()


class TaskSchema(ma.Schema):

    class Meta:
        model = Task
        fields = ('id', 'text', 'start_date', 'end_date', 'status', 'director_id', 'responsible_id')


task_schema = TaskSchema()
tasks_schema = TaskSchema(many=True)


class UserSchema(ma.Schema):

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'first_name', 'second_name', 'patronymic', 'age', 'sex', 'position',
                  'departament', 'privacy_policy')


user_schema = UserSchema()
users_schema = UserSchema(many=True)

